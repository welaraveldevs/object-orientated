<?php

/**
 * Overwriting System
 * 
*/
Class Sumon
{
	public function behavior()
	{
		return 'Sleep';
	}
}
Class Office extends Sumon
{
	public function behavior()
	{
		return 'Service Holder';
	}
}
Class Bus extends Sumon
{
	public function behavior()
	{
		return 'Passanger';
	}
}
$a = new Sumon();
$b = new Office();
$c = new Bus();

echo 'Init : Sumon is now '.$a->behavior().'<br>';
echo 'Extends Office : Sumon is now '.$b->behavior().'<br>';
echo 'Extends Bus : Sumon is now '.$c->behavior().'<br>';

?>
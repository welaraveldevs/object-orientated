<?php

Class Me
{
	public $name = 'Mesbaul';
	protected $mobile_1 = '01899999999';
	protected $mobile_2 = '01738120411';
	protected $house = '223/B, Dhaka';
	private $dateOfBirth = "21-12-1993";

	public function age()
	{
		$diff = date_diff(
			date_create($this->dateOfBirth), date_create(date("Y-m-d"))
		);
		return $diff->format('%y');
	}
}

Class Relative extends Me
{
	public function getHouse()
	{
		return $this->house;
	}


	public function getMobileNo()
	{
		return $this->mobile_1;
	}
}

Class Friends extends Me
{
	public function getMobileNo()
	{
		return $this->mobile_2;
	}
}


$me = new Me();
$relative = new Relative();
$friends = new Friends();


echo 'Public property name : ' . $me->name . '<br>';
echo 'Anyone know my age : ' . $me->age() . '<br>';

echo '---- Inheritance ----' . '<br>';
echo 'Relative know my house address : ' . $relative->getHouse() . '<br>';

echo '---- Polymorphism ----' . '<br>';
echo 'Relative call me : ' . $relative->getMobileNo() . '<br>';
echo 'Friends call me : ' . $friends->getMobileNo() . '<br>';




?>